# Front End Dev Assignment: Profile Edit

## About

This app is a simple front-end application that allows users to update their profile for reimbursement from companies they are interviewing with. Because a user might be interviewing with multiple companies, there is a dropdown in the upper left corner that allows a user to toggle between companies.

A user can click 'edit' on each section in order to edit that portion of the profile, broken down into Profile Details (about the user) and Payout Details (how they want to be paid). Payout details allow the user to choose if they want to be paid in USD, GBD, or EUR. It also allows the user to pick where their money will be deposited, via Paypal or Venmo, and asks for their email or phone number respectively.

You can view the live application [here](https://jessicajades.gitlab.io/reimbursement-profile-edit/)

## Tech

-   React
-   React-Select
-   Bootstrap
-   CSS
-   GitLab

## Improvements

In the interest of time, I chose to build this app using React and `create-react-app`, which is what I have the most experience building front-end applications with. This allowed me to focus on building a quality application within the time limit (~4 hours), without getting caught up in learning new tech. I plan on refactoring this application using Vue.js in order to gain more knowledge and experience with that framework.

I would also like to implement validation for checking valid email addresses, password lengths, etc.

## Install

```
git clone git@gitlab.com:jessicajades/reimbursement-profile-edit.git
cd reimbursement-profile-edit
npm install
npm start
```

## Author

Jessica Shepherd is a full-stack web developer currently based in Klamath Falls, OR. Where you can find her:

-   [jessicajade.dev](https://jessicajade.dev/)
-   [jessicajadecodes@gmail.com](mailto:jessicajadecodes@gmail.com)
-   [twitter.com/javascript_jess](https://twitter.com/javascript_jess)
-   [github.com/jessicajades](https://github.com/jessicajades)
