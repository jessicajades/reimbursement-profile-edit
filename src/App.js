import React from "react";
import "./styles.css";
import Sidebar from "./components/Sidebar";
import ProfileDetails from "./components/ProfileDetails";
import PaymentDetails from "./components/PaymentDetails";

function App() {
    return (
        <div className="App">
            <Sidebar />
            <div id="card" className="profile">
                <h1 id="profile-text">My Profile</h1>
                <div className="card-body">
                    <ProfileDetails />
                </div>
                <div className="card-body">
                    <PaymentDetails />
                </div>
            </div>
        </div>
    );
}

export default App;
