import React from "react";
import Select from "react-select";
import logo1 from "../logos/fantastico.png";
import logo2 from "../logos/compify.png";

const options = [
    {
        value: "fantastico",
        label: (
            <div>
                <img src={logo1} height="30px" width="30px" alt="logo" />
                Fantastico{" "}
            </div>
        ),
    },
    {
        value: "compify",
        label: (
            <div>
                <img src={logo2} height="30px" width="30px" alt="logo" />
                Compify{" "}
            </div>
        ),
    },
];

class Sidebar extends React.Component {
    render() {
        return (
            <div className="sidebar-wrapper">
                <Select
                    options={options}
                    id="company-dropdown"
                    defaultValue={{
                        label: (
                            <div>
                                <img
                                    src={logo1}
                                    height="30px"
                                    width="30px"
                                    alt="logo"
                                />
                                Fantastico{" "}
                            </div>
                        ),
                        value: "fantastico",
                    }}
                />

                <div className="nav-links">
                    <div className="link-group">
                        <a href="/" className="sidebar-link">
                            Reimbursement Policy
                        </a>
                        <a href="/" className="sidebar-link">
                            Expense Reports
                        </a>
                        <a href="/" className="sidebar-link">
                            Add New Expense
                        </a>
                    </div>

                    <div className="link-group">
                        <a href="/" className="sidebar-link">
                            My Profile
                        </a>
                        <a href="/" className="sidebar-link">
                            Notification Preferences
                        </a>
                        <a href="/" className="sidebar-link">
                            Log Out
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Sidebar;
