import React from "react";

class PaymentDetails extends React.Component {
    constructor() {
        super();

        this.state = {
            currency: "US Dollar - USD",
            method: "Paypal",
            phone: "555-342-0976",
            email: "jessicajadecodes@gmail.com",
            showForm: false,
        };
    }

    handleChange(e) {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ showForm: false });
    }

    handleAccountRender() {
        if (this.state.method === "Paypal") {
            return (
                <div>
                    <label>Paypal Account: </label>
                    <input
                        type="text"
                        value={this.state.email}
                        name="email"
                        className="form-control"
                        onChange={(e) => this.handleChange(e)}
                    />
                    <small>Please enter Paypal email</small>
                </div>
            );
        } else {
            return (
                <div>
                    <label>Venmo Account: </label>
                    <input
                        type="text"
                        value={this.state.phone}
                        name="phone"
                        className="form-control"
                        onChange={(e) => this.handleChange(e)}
                    />
                    <small>Please enter Venmo phone number</small>
                </div>
            );
        }
    }

    showForm() {
        return (
            <div>
                <form onSubmit={(e) => this.handleSubmit(e)}>
                    <div className="form-group">
                        <label>Payout Currency: </label>
                        <select
                            name="currency"
                            value={this.state.currency}
                            onChange={(e) => this.handleChange(e)}
                            className="form-control"
                        >
                            <option value="US Dollar - USD">
                                US Dollar - USD
                            </option>
                            <option value="British Pound - GBP">
                                British Pound - GBP
                            </option>
                            <option value="Euro - EUR">Euro - EUR</option>
                        </select>
                    </div>

                    <div className="form-group">
                        <label>Payout Method: </label>
                        <select
                            className="form-control"
                            name="method"
                            value={this.state.method}
                            onChange={(e) => this.handleChange(e)}
                        >
                            <option value="Paypal">Paypal</option>
                            <option value="Venmo">Venmo</option>
                        </select>
                    </div>

                    <div className="form-group">
                        {this.handleAccountRender()}
                    </div>

                    <input
                        name="submit"
                        type="submit"
                        className="btn btn-info form-button"
                    />
                    <a
                        href="https://jessicajades.gitlab.io/reimbursement-profile-edit/"
                        className="btn btn-danger form-button"
                    >
                        Cancel
                    </a>
                </form>
            </div>
        );
    }

    showInfo() {
        return (
            <div>
                <p>
                    <span className="label">Payout Currency:</span>{" "}
                    {this.state.currency}
                </p>
                <p>
                    <span className="label">Payout Method:</span>{" "}
                    {this.state.method}
                </p>

                {this.state.method === "Paypal" ? (
                    <p>
                        <span className="label">Paypal Account:</span>{" "}
                        {this.state.email}
                    </p>
                ) : (
                    <p>
                        <span className="label">Venmo Account:</span>{" "}
                        {this.state.phone}
                    </p>
                )}
                <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => this.setState({ showForm: true })}
                >
                    <i className="fa fa-pencil"></i> Edit Payment
                </button>
            </div>
        );
    }

    render() {
        return (
            <div>{this.state.showForm ? this.showForm() : this.showInfo()}</div>
        );
    }
}

export default PaymentDetails;
