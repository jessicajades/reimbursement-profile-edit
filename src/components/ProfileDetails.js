import React from "react";

class ProfileDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "Jessica Shepherd",
            email: "jessicajadecodes@gmail.com",
            password: "123456",
            showForm: false,
        };
    }

    handleChange(e) {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ showForm: false });
    }

    showForm = () => {
        return (
            <div>
                <form onSubmit={(e) => this.handleSubmit(e)}>
                    <div className="form-group">
                        <label>
                            <span className="label">Name:</span>{" "}
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            defaultValue={this.state.name}
                            name="name"
                            onChange={(e) => this.handleChange(e)}
                        />
                    </div>

                    <div className="form-group">
                        <label>
                            <span className="label">Email:</span>{" "}
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            defaultValue={this.state.email}
                            name="email"
                            onChange={(e) => this.handleChange(e)}
                        />
                    </div>

                    <div className="form-group password">
                        <label>
                            <span className="label">Password:</span>{" "}
                        </label>
                        <button className="btn btn-link" id="pw-button">
                            Edit password
                        </button>
                    </div>

                    <input
                        name="submit"
                        type="submit"
                        className="btn btn-info form-button"
                    />
                    <a
                        href="https://jessicajades.gitlab.io/reimbursement-profile-edit/"
                        className="btn btn-danger form-button"
                    >
                        {" "}
                        Cancel{" "}
                    </a>
                </form>
            </div>
        );
    };

    showInfo = () => {
        return (
            <div>
                <p>
                    <span className="label">Name:</span> {this.state.name}
                </p>
                <p>
                    <span className="label">Email:</span> {this.state.email}
                </p>
                <p>
                    <span className="label">Password:</span>{" "}
                    {"*".repeat(this.state.password.length)}
                </p>
                <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => this.setState({ showForm: true })}
                >
                    <i className="fa fa-pencil"></i> Edit Profile
                </button>
            </div>
        );
    };

    render() {
        return (
            <div>{this.state.showForm ? this.showForm() : this.showInfo()}</div>
        );
    }
}

export default ProfileDetails;
